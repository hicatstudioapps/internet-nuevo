package com.hicatstudio.internetfree.fragment;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.internal.LinkedTreeMap;
import com.hicatstudio.internetfree.R;
import com.hicatstudio.internetfree.ServiceGenerator;
import com.hicatstudio.internetfree.adapters.AppsRecyclerViewAdapter;
import com.hicatstudio.internetfree.fragment.dummy.DummyContent.DummyItem;
import com.hicatstudio.internetfree.interfaces.IApps;
import com.hicatstudio.internetfree.model.MoreAppsModel;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;

import at.grabner.circleprogress.CircleProgressView;
import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class AppsFragment extends Fragment {

    // TODO: Customize parameters
    private int mColumnCount = 1;

    private OnListFragmentInteractionListener mListener;
    @Bind(R.id.circleView)
    CircleProgressView circleProgressView;
    @Bind(R.id.list)
    RecyclerView recyclerView;
    @Bind(R.id.error)
    TextView error;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AppsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.app_fragment__list, container, false);
        ButterKnife.bind(this,view);
        error.setTypeface(Typeface.createFromAsset(getActivity().getAssets(),"t.ttf"));
        // Set the adapter
//        if (view instanceof RecyclerView) {
//            Context context = view.getContext();
//            RecyclerView recyclerView = (RecyclerView) view;
//            if (mColumnCount <= 1) {
//                recyclerView.setLayoutManager(new LinearLayoutManager(context));
//            } else {
//                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
//            }
//            recyclerView.setAdapter(new AppsRecyclerViewAdapter(DummyContent.ITEMS, mListener));
//        }
        return view;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(DummyItem item);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new Load().execute();
    }

    class Load extends AsyncTask<Void,Void,ArrayList<MoreAppsModel>>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            circleProgressView.spin();
        }

        @Override
        protected void onPostExecute(ArrayList<MoreAppsModel> moreAppsModels) {
            super.onPostExecute(moreAppsModels);
            if(moreAppsModels==null){
                circleProgressView.setVisibility(View.GONE);
                error.setVisibility(View.VISIBLE);
            }else {
                recyclerView.setAdapter(new AppsRecyclerViewAdapter(moreAppsModels, getContext()));
                circleProgressView.setVisibility(View.GONE);
            }
        }

        @Override
        protected ArrayList<MoreAppsModel> doInBackground(Void... voids) {
            ArrayList<MoreAppsModel> list= new ArrayList<>();
            IApps manuals= ServiceGenerator.createService(IApps.class);
            String url="http://api.ouchapps.com/hicatstudio/internetgratis/internet_apps.json";
//            url="http://192.168.101.1:8081/internet/a.txt";
            Call<Object> call= manuals.getApps(url);
            try {
                ArrayList data= (ArrayList) call.execute().body();
                for (Object ite :
                        data) {
                    LinkedTreeMap<String,String> app= (LinkedTreeMap<String, String>) ite;
                    url=app.get("app");
              //      url="http://192.168.101.1:8081/internet/r.html";
                    list.add(getAppDetails(url));
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            return list;
        }

        MoreAppsModel getAppDetails(final String appUrl) {

            MoreAppsModel app = new MoreAppsModel("", appUrl, "", "");


            try {
//                Document doc = Jsoup.connect(app.getAppUrl() + "&hl=" + Locale.getDefault()
//                        .getLanguage()).timeout(30000).get();
                Document doc = Jsoup.connect(app.getAppUrl()).timeout(30000).get();

                String description = doc.select("div[jsname=C4s9Ed]").first().ownText();

                Element img = doc.select("div.cover-container img").first();
                String urlImage = img.attr("src");
                if(urlImage.endsWith("-rw")){
                    urlImage = urlImage.replace("-rw", "");
                }

                Element name = doc.select("div.id-app-title").first();
                String appName = name.ownText();

                app.setAppImageUrl(urlImage);
                app.setAppName(appName);
                app.setAppDescription(description);
            }
            catch (Exception e) {
                e.printStackTrace();
            }

            return app;
        }
    }
}
