package com.hicatstudio.internetfree.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.hicatstudio.internetfree.R;
import com.hicatstudio.internetfree.ServiceGenerator;
import com.hicatstudio.internetfree.adapters.RecyclerViewAdapterideo;
import com.hicatstudio.internetfree.fragment.dummy.DummyContent.DummyItem;
import com.hicatstudio.internetfree.interfaces.Ivideos;

import java.util.ArrayList;

import at.grabner.circleprogress.CircleProgressView;
import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class VideoFragmen extends Fragment {

    // TODO: Customize parameters
    private int mColumnCount = 1;

    private OnListFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    @Bind(R.id.list)
    RecyclerView recyclerView;
    @Bind(R.id.circleView)
    CircleProgressView circleProgressView;
    RecyclerViewAdapterideo adapter;
    @Bind(R.id.error)
    TextView error;

    public VideoFragmen() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment__listideo, container, false);
        ButterKnife.bind(this,view);
        error.setTypeface(Typeface.createFromAsset(getActivity().getAssets(),"t.ttf"));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        circleProgressView.spin();
        if(adapter == null || adapter.getItemCount()==0){
            Ivideos manuals= ServiceGenerator.createService(Ivideos.class);
            String url="http://api.ouchapps.com/hicatstudio/internetgratis/internet_videos.json";
//            url="http://192.168.101.1:8081/internet/v.txt";
            Call<Object> call= manuals.getVideos(url);
            call.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    if(response.isSuccessful()){
                        Object data= response.body();
                        Log.d("", "onResponse: ");
                        recyclerView.setAdapter(new RecyclerViewAdapterideo((ArrayList)response.body(),getContext()));
                        circleProgressView.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    circleProgressView.setVisibility(View.GONE);
                    error.setVisibility(View.VISIBLE);
                }
            });
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(DummyItem item);
    }
}
