package com.hicatstudio.internetfree;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.drive.internal.CheckResourceIdsExistRequest;
import com.hicatstudio.internetfree.adapters.CountryAdapter;
import com.hicatstudio.internetfree.interfaces.ICountry;
import com.hicatstudio.internetfree.model.JsonCountry;

import at.grabner.circleprogress.CircleProgressView;
import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    Toolbar toolbar;
    RecyclerView recyclerView;
    CountryAdapter countryAdapter;
    CircleProgressView circleProgressView;
    TextView error;
    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // ButterKnife.bind(this);
        recyclerView= (RecyclerView)findViewById(R.id.countries);
        circleProgressView=(CircleProgressView)findViewById(R.id.circleView);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        error=(TextView)findViewById(R.id.error);
        error.setTypeface(Typeface.createFromAsset(getAssets(),"t.ttf"));
        final SpannableStringBuilder sb = new SpannableStringBuilder("INTERNET");
        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.WHITE);
        final StyleSpan bss = new StyleSpan(Typeface.BOLD);
        sb.setSpan(fcs, 0, 8, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        sb.setSpan(bss, 0, 8, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        sb.setSpan(new RelativeSizeSpan(1.5f),0,8,Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        Typeface font = Typeface.createFromAsset(getAssets(), "t.ttf");
        sb.setSpan(new CustomTypefaceSpan("" , font), 0 , 8,  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        toolbar.setTitle(sb);
        final SpannableStringBuilder sb1 = new SpannableStringBuilder("GRATIS");
        final ForegroundColorSpan fcs1 = new ForegroundColorSpan(Color.WHITE);
        final StyleSpan bss1 = new StyleSpan(Typeface.NORMAL);
        sb1.setSpan(fcs1, 0, 6, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        sb1.setSpan(bss1, 0, 6, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        sb1.setSpan(new RelativeSizeSpan(1f),0,6,Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        Typeface font1 = Typeface.createFromAsset(getAssets(), "t.ttf");
        sb1.setSpan(new CustomTypefaceSpan("" , font), 0 , 6,  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        toolbar.setSubtitle(sb1);
        toolbar.setNavigationIcon(R.mipmap.ic_launcher);
        setSupportActionBar(toolbar);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        loadAll();
        AdRequest var13;
        this.adView = (AdView)this.findViewById(R.id.adView);
        var13 = (new AdRequest.Builder()).build();
        this.adView.loadAd(var13);
        this.adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                adView.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem var1) {
        boolean var4;
        switch (var1.getItemId()){
//            case R.id.img_favo:
//                Intent var2 = new Intent(CategoryMain_Activity.this.getApplicationContext(), Favourite_Activity.class);
//                CategoryMain_Activity.this.startActivity(var2);
//                break;
            case R.id.action_rate:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+getPackageName().toString())));
                break;
        }
        return true;
    }

    void loadAll(){
        ICountry iCountry= ServiceGenerator.createService(ICountry.class);
        String url="http://wildercs.net/wilder/app1/api.php";
//        url= "http://192.168.101.1:8081/internet/list.txt";
        Call<JsonCountry> call= iCountry.getCountries(url);
        circleProgressView.spin();
        call.enqueue(new Callback<JsonCountry>() {
            @Override
            public void onResponse(Call<JsonCountry> call, Response<JsonCountry> response) {
                if(response.isSuccessful()){
                   countryAdapter = new CountryAdapter(response.body().getSailing(),MainActivity.this);
                    recyclerView.setAdapter(countryAdapter);
                    circleProgressView.setVisibility(View.GONE);
                }
                else
                   error.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<JsonCountry> call, Throwable t) {
                circleProgressView.setVisibility(View.GONE);
               error.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home,menu);
        return true;
    }
}
