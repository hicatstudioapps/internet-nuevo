package com.hicatstudio.internetfree.interfaces;

import com.hicatstudio.internetfree.model.JsonCountry;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by CQ on 01/08/2016.
 */
public interface ICountry {
    @GET
    Call<JsonCountry> getCountries(@Url String url);
}
