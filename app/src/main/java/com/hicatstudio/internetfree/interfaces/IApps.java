package com.hicatstudio.internetfree.interfaces;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by CQ on 02/08/2016.
 */
public interface IApps {

    @GET
    Call<Object> getApps(@Url String url);
}
