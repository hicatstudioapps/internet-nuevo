package com.hicatstudio.internetfree.interfaces;

import com.hicatstudio.internetfree.model.ManualList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by CQ on 02/08/2016.
 */
public interface Ivideos {
    @GET
    Call<Object> getVideos(@Url String url);
}
