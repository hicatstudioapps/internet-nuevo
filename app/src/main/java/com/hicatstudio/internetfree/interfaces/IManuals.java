package com.hicatstudio.internetfree.interfaces;

import com.hicatstudio.internetfree.model.ManualList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by CQ on 01/08/2016.
 */
public interface IManuals {
    @GET
    Call<ManualList> getManuals(@Url String url);
}
