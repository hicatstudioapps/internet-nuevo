package com.hicatstudio.internetfree;

import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.ErrorReason;
import com.google.android.youtube.player.YouTubePlayer.PlayerStateChangeListener;
import com.google.android.youtube.player.YouTubePlayerView;

import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

/**
 * A simple YouTube Android API demo application which shows how to create a
 * simple application that displays a YouTube Video in a
 * {@link YouTubePlayerView}.
 * <p>
 * Note, to use a {@link YouTubePlayerView}, your activity must extend
 * {@link YouTubeBaseActivity}.
 */
public class ReproductorYoutube extends YouTubeFailureRecoveryActivity {

	private String urlVideo;
	private YouTubePlayer player;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.v_youtube);
		
        if ( getIntent() != null && getIntent().getData() != null) {
        	
        	urlVideo = getIntent().getData().toString();
        	
        } else {
        	
        	Toast.makeText(this, "Error cargando video", Toast.LENGTH_LONG).show();
        	finish();
        	return;
        	
        }
		YouTubePlayerView youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
		youTubeView.initialize("AIzaSyD3VMFqpu0w_YvgSq0OdlGDbNNNH_4BkIs", this);

	}
	String getYouTubeID (String url) {

		String pattern = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|watch\\?v%3D|%2Fvideos%2F|embed%‌​2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\n]*";

		Pattern compiledPattern = Pattern.compile(pattern);
		Matcher matcher = compiledPattern.matcher(url);

		if( matcher.find() ){

			String id = matcher.group();

			if ( id.length() == 11 ) {

				return id;

			} else {

				return null;

			}

		} else {

			return null;
		}

	}
	@Override
	public void onInitializationSuccess(YouTubePlayer.Provider provider, 
										YouTubePlayer youTubePlayer, boolean wasRestored) {
		if (!wasRestored) {
			player=youTubePlayer;
			player.cueVideo(getYouTubeID(urlVideo));
			player.setPlayerStateChangeListener(new PlayerStateChangeListener() {

				@Override
				public void onLoaded(String videoId) {
					player.play();
				}

				@Override
				public void onAdStarted() {

				}

				@Override
				public void onError(ErrorReason arg0) {

				}

				@Override
				public void onLoading() {

				}

				@Override
				public void onVideoEnded() {

				}

				@Override
				public void onVideoStarted() {

				}
			});
		}
	}

	@Override
	protected YouTubePlayer.Provider getYouTubePlayerProvider() {
		return (YouTubePlayerView) findViewById(R.id.youtube_view);
	}


	
	
	
	/**
	 * Método para extraer la ID del vídeo de YouTube a partir de una URL.
	 * 
	 * DEPRECADO - Utilizar el método Utils.getYouTubeID(urlVideo).
	 * 
	 * @param url
	 * @return
	 */
	public static String extractYoutubeIdx(String url) {
		String id = url;
		try {
			String query = new URL(url).getQuery();
			if (query != null) {
				String[] param = query.split("&");
				for (String row : param) {
					String[] param1 = row.split("=");
					if (param1[0].equals("v")) {
						id = param1[1];
					}
				}
			} else {
				if (url.contains("embed")) {
					id = url.substring(url.lastIndexOf("/") + 1);
				}
			}
		} catch (Exception ex) {
			Log.e("Exception", ex.toString());
			id = url;
		}
		return id;
	}

}
