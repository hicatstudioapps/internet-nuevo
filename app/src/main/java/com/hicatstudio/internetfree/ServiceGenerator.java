package com.hicatstudio.internetfree;

import java.io.FileWriter;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by CQ on 22/03/2016.
 */

public class ServiceGenerator  {
//    private static final String BASE_URL = "http://www.google.com.cu/";
    private static final String BASE_URL = "http://192.168.101.1:8081/";

             private static Retrofit.Builder builder =
             new Retrofit.Builder()
             .baseUrl(BASE_URL)
                     .addConverterFactory(GsonConverterFactory.create());
             private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                     .connectTimeout(10, TimeUnit.SECONDS)
                     .readTimeout(30,TimeUnit.SECONDS);

             public static <S> S createService(Class<S> serviceClass) {

         builder.client(httpClient.build());
         Retrofit retrofit = builder.build();
         return retrofit.create(serviceClass);
         }
}
