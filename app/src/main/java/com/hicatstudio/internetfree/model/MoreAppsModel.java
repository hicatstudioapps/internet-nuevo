package com.hicatstudio.internetfree.model;

import java.io.Serializable;

import android.text.TextUtils;

/**
 * Model that represents an app
 * 
 * @author Junior Buckeridge A.
 *
 */
public class MoreAppsModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6061053059070761391L;
	
	private String appName;
	private String appUrl;
	private String appDescription;
	private String appImageUrl;
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getAppUrl() {
		return appUrl;
	}
	public void setAppUrl(String appUrl) {
		this.appUrl = appUrl;
	}
	public String getAppDescription() {
		return appDescription;
	}
	public void setAppDescription(String appDescription) {
		this.appDescription = appDescription;
	}
	public String getAppImageUrl() {
		return appImageUrl;
	}
	public void setAppImageUrl(String appImageUrl) {
		this.appImageUrl = appImageUrl;
	}
	
	public MoreAppsModel(){
		
	}
	
	public MoreAppsModel(String appName, String appUrl, String appDescription,
			String appImageUrl) {
		super();
		this.appName = appName;
		this.appUrl = appUrl;
		this.appDescription = appDescription;
		this.appImageUrl = appImageUrl;
	}
	
	@Override
	public boolean equals(Object o) {
		super.equals(o);
		
		try {
			MoreAppsModel m = (MoreAppsModel) o;
			if(TextUtils.equals(appUrl, m.getAppUrl())
				/* && TextUtils.equals(appName, m.getAppName())
				&& TextUtils.equals(appDescription, m.getAppDescription())
				&& TextUtils.equals(appImageUrl, m.getAppImageUrl()) */ ){
				
				return true;
			}
			
		} catch (Exception e) {
		}
		
		return false;
	}
	
}
