package com.hicatstudio.internetfree.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by CQ on 01/08/2016.
 */
public class ManualList {
    @SerializedName("Projects_App")
    @Expose
    private ArrayList<ProjectsApp> projectsApp = new ArrayList<ProjectsApp>();

    /**
     *
     * @return
     *     The projectsApp
     */
    public ArrayList<ProjectsApp> getProjectsApp() {
        return projectsApp;
    }

    /**
     *
     * @param projectsApp
     *     The Projects_App
     */
    public void setProjectsApp(ArrayList<ProjectsApp> projectsApp) {
        this.projectsApp = projectsApp;
    }
}
