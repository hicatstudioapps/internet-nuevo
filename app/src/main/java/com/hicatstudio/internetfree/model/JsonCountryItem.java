package com.hicatstudio.internetfree.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by CQ on 01/08/2016.
 */
public class JsonCountryItem {
    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCategory_image() {
        return category_image;
    }

    public void setCategory_image(String category_image) {
        this.category_image = category_image;
    }

    @SerializedName("cid")

    @Expose
    private String cid;
    @SerializedName("category_name")
    @Expose
    private String category_name;
    @SerializedName("category_image")
    @Expose
    private String category_image;
}
