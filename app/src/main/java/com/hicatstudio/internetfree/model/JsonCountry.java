package com.hicatstudio.internetfree.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CQ on 01/08/2016.
 */
public class JsonCountry {
    @SerializedName("Projects_App")
    @Expose
    private ArrayList<JsonCountryItem> countrys = new ArrayList<JsonCountryItem>();

    /**
     *
     * @return
     *     The sailing
     */
    public ArrayList<JsonCountryItem> getSailing() {
        return countrys;
    }

    /**
     *
     * @param sailing
     *     The sailing
     */
    public void setSailing(ArrayList<JsonCountryItem> sailing) {
        this.countrys = sailing;
    }
}
