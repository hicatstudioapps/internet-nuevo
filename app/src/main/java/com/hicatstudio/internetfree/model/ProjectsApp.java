package com.hicatstudio.internetfree.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by CQ on 01/08/2016.
 */
public class ProjectsApp implements Serializable{
    @SerializedName("project_pid")
    @Expose
    private String projectPid;
    @SerializedName("project_cid")
    @Expose
    private String projectCid;
    @SerializedName("project_name")
    @Expose
    private String projectName;
    @SerializedName("project_image")
    @Expose
    private String projectImage;
    @SerializedName("project_intro")
    @Expose
    private String projectIntro;
    @SerializedName("project_materials")
    @Expose
    private String projectMaterials;
    @SerializedName("project_procedure")
    @Expose
    private String projectProcedure;
    @SerializedName("project_conclusion")
    @Expose
    private String projectConclusion;
    @SerializedName("project_status")
    @Expose
    private String projectStatus;
    @SerializedName("cid")
    @Expose
    private String cid;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("category_image")
    @Expose
    private String categoryImage;
    @SerializedName("rating_promedio")
    @Expose
    private String ratingPromedio;
    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     *     The projectPid
     */
    public String getProjectPid() {
        return projectPid;
    }

    /**
     *
     * @param projectPid
     *     The project_pid
     */
    public void setProjectPid(String projectPid) {
        this.projectPid = projectPid;
    }

    /**
     *
     * @return
     *     The projectCid
     */
    public String getProjectCid() {
        return projectCid;
    }

    /**
     *
     * @param projectCid
     *     The project_cid
     */
    public void setProjectCid(String projectCid) {
        this.projectCid = projectCid;
    }

    /**
     *
     * @return
     *     The projectName
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     *
     * @param projectName
     *     The project_name
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    /**
     *
     * @return
     *     The projectImage
     */
    public String getProjectImage() {
        return projectImage;
    }

    /**
     *
     * @param projectImage
     *     The project_image
     */
    public void setProjectImage(String projectImage) {
        this.projectImage = projectImage;
    }

    /**
     *
     * @return
     *     The projectIntro
     */
    public String getProjectIntro() {
        return projectIntro;
    }

    /**
     *
     * @param projectIntro
     *     The project_intro
     */
    public void setProjectIntro(String projectIntro) {
        this.projectIntro = projectIntro;
    }

    /**
     *
     * @return
     *     The projectMaterials
     */
    public String getProjectMaterials() {
        return projectMaterials;
    }

    /**
     *
     * @param projectMaterials
     *     The project_materials
     */
    public void setProjectMaterials(String projectMaterials) {
        this.projectMaterials = projectMaterials;
    }

    /**
     *
     * @return
     *     The projectProcedure
     */
    public String getProjectProcedure() {
        return projectProcedure;
    }

    /**
     *
     * @param projectProcedure
     *     The project_procedure
     */
    public void setProjectProcedure(String projectProcedure) {
        this.projectProcedure = projectProcedure;
    }

    /**
     *
     * @return
     *     The projectConclusion
     */
    public String getProjectConclusion() {
        return projectConclusion;
    }

    /**
     *
     * @param projectConclusion
     *     The project_conclusion
     */
    public void setProjectConclusion(String projectConclusion) {
        this.projectConclusion = projectConclusion;
    }

    /**
     *
     * @return
     *     The projectStatus
     */
    public String getProjectStatus() {
        return projectStatus;
    }

    /**
     *
     * @param projectStatus
     *     The project_status
     */
    public void setProjectStatus(String projectStatus) {
        this.projectStatus = projectStatus;
    }

    /**
     *
     * @return
     *     The cid
     */
    public String getCid() {
        return cid;
    }

    /**
     *
     * @param cid
     *     The cid
     */
    public void setCid(String cid) {
        this.cid = cid;
    }

    /**
     *
     * @return
     *     The categoryName
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     *
     * @param categoryName
     *     The category_name
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     *
     * @return
     *     The categoryImage
     */
    public String getCategoryImage() {
        return categoryImage;
    }

    /**
     *
     * @param categoryImage
     *     The category_image
     */
    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    /**
     *
     * @return
     *     The ratingPromedio
     */
    public String getRatingPromedio() {
        return ratingPromedio;
    }

    /**
     *
     * @param ratingPromedio
     *     The rating_promedio
     */
    public void setRatingPromedio(String ratingPromedio) {
        this.ratingPromedio = ratingPromedio;
    }
}
