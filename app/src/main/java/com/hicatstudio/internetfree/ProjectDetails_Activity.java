package com.hicatstudio.internetfree;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.hicatstudio.internetfree.model.ProjectsApp;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("ResourceType")
public class ProjectDetails_Activity extends AppCompatActivity {
  String allArrayprocatename;
   String allArrayprocid;
   String allArrayproconclu;
   String allArrayproimage;
   String allArrayprointro;
   String allArraypromate;
   String allArrayproname;
   String allArraypropid;
   String allArrayproproce;
   String allArrayrating;
   ArrayList arrayOfProjectDetails;
   private DisplayImageOptions options;
   ImageView backicon;
   public DatabaseHandler db;
   ImageView imgfavourite;
   ImageView imgproject;
   ImageView imgshare;
   private AdView mAdView;
   CharSequence mensaje;
   int position;
   TextView txtRating;
   TextView txtprojecttitle;
   RatingBar votarpost;
   WebView webviewprojectconclusion;
   WebView webviewprojectintro;
   WebView webviewprojectprocedi;
   WebView webviewrequi;
   private Toolbar toolbar;
   Menu menu;
   private AdView adView;

   public void ReciveMensaje(String var1) {
      this.mensaje = var1;
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      getMenuInflater().inflate(R.menu.detail,menu);
      List var4 = this.db.getFavRow(this.allArraypropid);
      if(var4.size() == 0) {
         menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.ic_action_fav));
      } else {
         menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.fav_hover));
         //  this.imgfavourite.setImageDrawable(getResources().getDrawable(R.drawable.fav_hover));
      }
      this.menu=menu;
      return true;
   }

   public void agregarVotosRatingBar() {
      try {
         this.votarpost.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar var1, float var2, boolean var3) {
               //ProjectDetails_Activity.this.gr.Ejecutar("registrar_rating", ProjectDetails_Activity.this.allArraypropid.toString(), String.valueOf(ProjectDetails_Activity.this.votarpost.getRating()));
               Toast.makeText(ProjectDetails_Activity.this, "Valoro el Post con " + String.valueOf(ProjectDetails_Activity.this.votarpost.getRating()) + " Estrellas", 0).show();
            }
         });
      } catch (Exception var2) {
         Toast.makeText(this, "Error ratingbar " + var2.getMessage(), 1).show();
      }
   }

   protected void onCreate(Bundle var1) {
      this.requestWindowFeature(1);
      super.onCreate(var1);
      this.options = new DisplayImageOptions.Builder()
              .cacheInMemory(true)
              .showImageOnLoading(R.mipmap.ic_launcher)
              .showImageForEmptyUri(R.mipmap.ic_launcher)
              .showImageOnFail(R.mipmap.ic_launcher)
              .bitmapConfig(Bitmap.Config.RGB_565)
              .build();
      this.setContentView(R.layout.projectdetails_fragment);
      toolbar=(Toolbar)findViewById(R.id.toolbar);
      final SpannableStringBuilder sb = new SpannableStringBuilder("INTERNET");
      final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.WHITE);
      final StyleSpan bss = new StyleSpan(Typeface.BOLD);
      sb.setSpan(fcs, 0, 8, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
      sb.setSpan(bss, 0, 8, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
      sb.setSpan(new RelativeSizeSpan(1.5f),0,8,Spannable.SPAN_INCLUSIVE_INCLUSIVE);
      Typeface font = Typeface.createFromAsset(getAssets(), "t.ttf");
      sb.setSpan(new CustomTypefaceSpan("" , font), 0 , 8,  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
      toolbar.setTitle(sb);
      final SpannableStringBuilder sb1 = new SpannableStringBuilder("GRATIS");
      final ForegroundColorSpan fcs1 = new ForegroundColorSpan(Color.WHITE);
      final StyleSpan bss1 = new StyleSpan(Typeface.NORMAL);
      sb1.setSpan(fcs1, 0, 6, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
      sb1.setSpan(bss1, 0, 6, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
      sb1.setSpan(new RelativeSizeSpan(1f),0,6,Spannable.SPAN_INCLUSIVE_INCLUSIVE);
      Typeface font1 = Typeface.createFromAsset(getAssets(), "t.ttf");
      sb1.setSpan(new CustomTypefaceSpan("" , font), 0 , 6,  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
      toolbar.setSubtitle(sb1);
      setSupportActionBar(
              toolbar
      );
      AdRequest var13;

      this.adView = (AdView)this.findViewById(R.id.adView);
      var13 = (new AdRequest.Builder()).build();
      this.adView.loadAd(var13);
      this.adView.setAdListener(new AdListener() {
         @Override
         public void onAdLoaded() {
            super.onAdLoaded();
            adView.setVisibility(View.VISIBLE);
         }
      });
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setHomeButtonEnabled(true);
      this.mAdView = (AdView)this.findViewById(R.id.adView);
      this.mAdView.loadAd((new AdRequest.Builder()).build());
      this.db = new DatabaseHandler(this.getApplicationContext());
      this.backicon = (ImageView)this.findViewById(R.id.img_back);
      this.imgfavourite = (ImageView)this.findViewById(R.id.img_projectfavo);
      this.imgshare = (ImageView)this.findViewById(R.id.img_share);
      this.imgproject = (ImageView)this.findViewById(R.id.img_project);
      this.webviewprojectintro = (WebView)this.findViewById(R.id.projectintro_text);
      this.webviewrequi = (WebView)this.findViewById(R.id.projectmaterials_text);
      this.webviewprojectprocedi = (WebView)this.findViewById(R.id.projectprocedure_text);
      this.webviewprojectconclusion = (WebView)this.findViewById(R.id.project_conclusion_text);
      this.txtprojecttitle = (TextView)this.findViewById(R.id.project_title);
      this.txtRating = (TextView)this.findViewById(R.id.textViewRating);
      this.votarpost = (RatingBar)this.findViewById(R.id.rating);
      this.votarpost.setNumStars(5);
      this.votarpost.setMax(5);
//      ((LayerDrawable)this.votarpost.getProgressDrawable()).getDrawable(2).setColorFilter(Color.parseColor("#ffffff"), Mode.SRC_ATOP);
//      this.agregarVotosRatingBar();
      Intent var2 = this.getIntent();
      ProjectsApp manual= (ProjectsApp) var2.getSerializableExtra("o");
      this.position = var2.getIntExtra("POSITION", 0);
      this.allArraypropid = manual.getProjectPid();
      this.allArrayprocid = manual.getProjectCid();
      this.allArrayproname = manual.getProjectName();
      this.allArrayproimage = manual.getProjectImage();
      this.allArrayprointro = manual.getProjectIntro();
      this.allArraypromate = manual.getProjectMaterials();
      this.allArrayproproce = manual.getProjectProcedure();
      this.allArrayproconclu = manual.getProjectConclusion();
      this.allArrayrating = manual.getRatingPromedio();
      ImageLoader.getInstance().displayImage(Uri.parse("http://wildercs.net/wilder/app1/images/" + this.allArrayproimage).toString(), imgproject, options, new SimpleImageLoadingListener() {
         @Override
         public void onLoadingStarted(String imageUri, View view) {
         }

         @Override
         public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
         }

         @Override
         public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
         }
      });
     // this.imageLoaderr.DisplayImage("http://wildercs.net/wilder/app1/images/" + this.allArrayproimage, this.imgproject);
      this.txtprojecttitle.setText(this.allArrayproname);
      this.txtRating.setText(" Rating: " + String.valueOf((float) Math.round(Float.parseFloat(this.allArrayrating) * 100.0F) / 100.0F));
      Html.fromHtml(this.allArrayprointro).toString();
      this.webviewprojectintro.setBackgroundColor(getResources().getColor(android.R.color.transparent));
      this.webviewprojectintro.setFocusableInTouchMode(false);
      this.webviewprojectintro.setFocusable(false);
      this.webviewprojectintro.getSettings().setDefaultTextEncodingName("UTF-8");
      String var3 = this.allArrayprointro;
      var3 = "<html><head><style type=\"text/css\">body{color: #ffffff;}</style></head><body>" + var3 + "</body></html>";
      this.webviewprojectintro.loadData(var3, "text/html", "utf-8");
      this.webviewrequi.setBackgroundColor(getResources().getColor(android.R.color.transparent));
      this.webviewrequi.setFocusableInTouchMode(false);
      this.webviewrequi.setFocusable(false);
      this.webviewrequi.getSettings().setDefaultTextEncodingName("UTF-8");
      var3 = this.allArraypromate;
      var3 = "<html><head><style type=\"text/css\">body{color: #ffffff;}</style></head><body>" + var3 + "</body></html>";
      this.webviewrequi.loadData(var3, "text/html", "utf-8");
      this.webviewprojectprocedi.setBackgroundColor(getResources().getColor(android.R.color.transparent));
      this.webviewprojectprocedi.setFocusableInTouchMode(false);
      this.webviewprojectprocedi.setFocusable(false);
      this.webviewprojectprocedi.getSettings().setDefaultTextEncodingName("UTF-8");
      var3 = this.allArrayproproce;
      var3 = "<html><head><style type=\"text/css\">body{color: #ffffff;}</style></head><body>" + var3 + "</body></html>";
      this.webviewprojectprocedi.loadData(var3, "text/html", "utf-8");
      this.webviewprojectconclusion.setBackgroundColor(getResources().getColor(android.R.color.transparent));
      this.webviewprojectconclusion.setFocusableInTouchMode(false);
      this.webviewprojectconclusion.setFocusable(false);
      this.webviewprojectconclusion.getSettings().setDefaultTextEncodingName("UTF-8");
      var3 = this.allArrayproconclu;
      var3 = "<html><head><style type=\"text/css\">body{color: #ffffff;}</style></head><body>" + var3 + "</body></html>";
      this.webviewprojectconclusion.loadData(var3, "text/html", "utf-8");


      this.backicon.setOnClickListener(new OnClickListener() {
         public void onClick(View var1) {
            ProjectDetails_Activity.this.onBackPressed();
         }
      });
      this.imgfavourite.setOnClickListener(new OnClickListener() {
         public void onClick(View var1) {
            List var2 = ProjectDetails_Activity.this.db.getFavRow(ProjectDetails_Activity.this.allArraypropid);
            if(var2.size() == 0) {
               Toast.makeText(ProjectDetails_Activity.this.getApplicationContext(), "Agregado a Favoritos", 0).show();
               ProjectsApp var3=new ProjectsApp();
               var3.setProjectPid(allArraypropid);
               var3.setProjectCid(allArrayprocid);
               var3.setProjectName(allArrayproname);
               var3.setProjectImage(allArrayproimage);
               var3.setProjectIntro(allArrayprointro);
               var3.setProjectMaterials(allArraypromate);
               var3.setProjectProcedure(allArrayproproce);
               var3.setProjectConclusion(allArrayproconclu);
               var3.setRatingPromedio(allArrayrating);
               ProjectDetails_Activity.this.db.AddtoFavorite(var3);
               menu.getItem(0).setIcon(R.drawable.fav);
               ProjectDetails_Activity.this.imgfavourite.setImageDrawable(getResources().getDrawable(R.drawable.fav_hover));
            } else if(((ProjectsApp)var2.get(0)).getProjectPid().equals(ProjectDetails_Activity.this.allArraypropid)) {
               ProjectDetails_Activity.this.db.RemoveFav(allArraypropid);
               Toast.makeText(ProjectDetails_Activity.this.getApplicationContext(), "Eliminado de Favoritos", 0).show();
               ProjectDetails_Activity.this.imgfavourite.setImageDrawable(getResources().getDrawable(R.drawable.fav));
            }

         }
      });
      this.imgshare.setOnClickListener(new OnClickListener() {
         String descripcion;

         public void onClick(View var1) {
            this.descripcion = ProjectDetails_Activity.this.allArrayprointro;
            String var4 = Html.fromHtml(this.descripcion).toString();
            String var6 = Html.fromHtml(ProjectDetails_Activity.this.allArraypromate).toString();
            String var3 = Html.fromHtml(ProjectDetails_Activity.this.allArrayproproce).toString();
            String var5 = Html.fromHtml(ProjectDetails_Activity.this.allArrayproconclu).toString();
            Intent var2 = new Intent();
            var2.setAction("android.intent.action.SEND");
            var2.putExtra("android.intent.extra.TEXT", "Detalles del Metodo: \n \n Titulo: " + ProjectDetails_Activity.this.txtprojecttitle.getText().toString() + "\n \n Descripcion: " + var4 + "\n Requisitos: " + var6 + "\n Procedimiento: " + var3 + "\n Resultados: " + var5 + "\n " + " Me gustar�a compartir esto con ustedes. Aqu� se puede descargar esta aplicaci�n desde playstore " + "https://play.google.com/store/apps/details?id=" + ProjectDetails_Activity.this.getPackageName());
            var2.setType("text/plain");
            ProjectDetails_Activity.this.startActivity(var2);
         }
      });
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      switch (item.getItemId()){
         case R.id.img_projectfavo:
            List var2 = ProjectDetails_Activity.this.db.getFavRow(ProjectDetails_Activity.this.allArraypropid);
            if(var2.size() == 0) {
               Toast.makeText(ProjectDetails_Activity.this.getApplicationContext(), "Agregado a Favoritos", 0).show();
               ProjectsApp var3=new ProjectsApp();
               var3.setProjectPid(allArraypropid);
               var3.setProjectCid(allArrayprocid);
               var3.setProjectName(allArrayproname);
               var3.setProjectImage(allArrayproimage);
               var3.setProjectIntro(allArrayprointro);
               var3.setProjectMaterials(allArraypromate);
               var3.setProjectProcedure(allArrayproproce);
               var3.setProjectConclusion(allArrayproconclu);
               var3.setRatingPromedio(allArrayrating);
               ProjectDetails_Activity.this.db.AddtoFavorite(var3);
               ProjectDetails_Activity.this.imgfavourite.setImageDrawable(getResources().getDrawable(R.drawable.fav_hover));
               menu.getItem(0).setIcon(R.drawable.fav_hover);
            } else if(((ProjectsApp)var2.get(0)).getProjectPid().equals(ProjectDetails_Activity.this.allArraypropid)) {
               ProjectDetails_Activity.this.db.RemoveFav(allArraypropid);
               menu.getItem(0).setIcon(R.drawable.fav);
               Toast.makeText(ProjectDetails_Activity.this.getApplicationContext(), "Eliminado de Favoritos", 0).show();
               ProjectDetails_Activity.this.imgfavourite.setImageDrawable(getResources().getDrawable(R.drawable.fav));
            }
            break;
         case R.id.img_share:
            String descripcion= ProjectDetails_Activity.this.allArrayprointro;
            String var4 = Html.fromHtml(descripcion).toString();
            String var6 = Html.fromHtml(ProjectDetails_Activity.this.allArraypromate).toString();
            String var3 = Html.fromHtml(ProjectDetails_Activity.this.allArrayproproce).toString();
            String var5 = Html.fromHtml(ProjectDetails_Activity.this.allArrayproconclu).toString();
            Intent intent = new Intent();
            intent.setAction("android.intent.action.SEND");
            intent.putExtra("android.intent.extra.TEXT", "Detalles del Metodo: \n \n Titulo: " + ProjectDetails_Activity.this.txtprojecttitle.getText().toString() + "\n \n Descripcion: " + var4 + "\n Requisitos: " + var6 + "\n Procedimiento: " + var3 + "\n Resultados: " + var5 + "\n " + " Me gustar�a compartir esto con ustedes. Aqu� se puede descargar esta aplicaci�n desde playstore " + "https://play.google.com/store/apps/details?id=" + ProjectDetails_Activity.this.getPackageName());
            intent.setType("text/plain");
            ProjectDetails_Activity.this.startActivity(intent);

            break;
         case android.R.id.home:
            this.onBackPressed();
            break;
      }
      return true;
   }
}
