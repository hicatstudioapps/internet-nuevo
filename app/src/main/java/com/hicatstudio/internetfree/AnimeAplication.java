package com.hicatstudio.internetfree;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.onesignal.OneSignal;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

/**
 * Created by CQ on 02/03/2016.
 */
public class AnimeAplication extends MultiDexApplication {

    private AdRequest request;
    public static InterstitialAd interstitialAd;
    public static SharedPreferences pref;

    @Override
    public void onCreate() {
        super.onCreate();
        OneSignal.startInit(this).init();
        pref= getSharedPreferences("interistial",MODE_PRIVATE);
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
         .cacheInMemory(true)
                .cacheOnDisk(true)
//                .showImageOnLoading(R.drawable.empty)
//                .showImageOnFail(R.drawable.empty)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
        .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                //.memoryCacheExtraOptions(480,480)
                .defaultDisplayImageOptions(defaultOptions)
        .build();
        ImageLoader.getInstance().init(config);
        interstitialAd=new InterstitialAd(this);
        interstitialAd.setAdUnitId("ca-app-pub-1502760063615827/2726174191");
        request= new AdRequest.Builder().build();
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                interstitialAd.loadAd(request);
                SharedPreferences pref= getSharedPreferences("interistial",MODE_PRIVATE);
                pref.edit().putLong("date",new DateTime().getMillis()).commit();
            }
        });
        interstitialAd.loadAd(request);

    }

    public static void showInterstitial(){
        if(interstitialAd.isLoaded()){
            long date= pref.getLong("date",0);
            DateTime current= new DateTime();
            DateTime old=null;
            if(date>0){
                old= new DateTime(date);
            }else {
                pref.edit().putLong("date",new DateTime().getMillis()).commit();
                old= new DateTime();
            }
            int minutes=new Period(old,current).getMinutes();
            if(old!=null && new Period(old,current).getMinutes()>2){
                Log.d("mostrar inerstial","ghghg");

            interstitialAd.show();
            }
        else
                Log.d("no mostrar inerstial","ghghhh");
        }
    }
    public static void test(){
        pref.edit().putLong("date",new DateTime().getMillis()).commit();
    }
}
