package com.hicatstudio.internetfree.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.hicatstudio.internetfree.AnimeAplication;
import com.hicatstudio.internetfree.R;
import com.hicatstudio.internetfree.model.MoreAppsModel;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;


public class AppsRecyclerViewAdapter extends RecyclerView.Adapter<AppsRecyclerViewAdapter.ViewHolder> {

    ArrayList<MoreAppsModel> items;
    Context context;

    public AppsRecyclerViewAdapter(ArrayList<MoreAppsModel> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_projectsapp, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.text.setText(items.get(position).getAppName());
        holder.rating.setVisibility(View.INVISIBLE);
        ImageLoader.getInstance().displayImage("http:"+items.get(position).getAppImageUrl(),holder.imageView);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {
        @Bind(R.id.img_categorylist)
        ImageView imageView;
        @Bind(R.id.text_catelist_imgtitle)
        TextView text;
        @Bind(R.id.ratingBar1)
        RatingBar rating;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(items.get(getAdapterPosition()).getAppUrl())));
            AnimeAplication.showInterstitial();
        }
    }
}
