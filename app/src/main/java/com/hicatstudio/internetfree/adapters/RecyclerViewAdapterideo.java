package com.hicatstudio.internetfree.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.internal.LinkedTreeMap;
import com.hicatstudio.internetfree.AnimeAplication;
import com.hicatstudio.internetfree.R;
import com.hicatstudio.internetfree.ReproductorYoutube;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;


public class RecyclerViewAdapterideo extends RecyclerView.Adapter<RecyclerViewAdapterideo.ViewHolder> {

    ArrayList<Object> item;
    Context context;

    public RecyclerViewAdapterideo(ArrayList<Object> item, Context context) {
        this.item = item;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_projectsapp, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        LinkedTreeMap<String,String> data= (LinkedTreeMap<String, String>) item.get(position);
        holder.text.setText(data.get("title"));
        holder.rating.setRating(Float.parseFloat(data.get("rating")));
        String urlImage= "http://img.youtube.com/vi/"+getYouTubeID(data.get("video"))+"/1.jpg";
        ImageLoader.getInstance().displayImage(urlImage, holder.imageView, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {

            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
//                ImageView image= (ImageView) view;
//                RelativeLayout.LayoutParams lp= (RelativeLayout.LayoutParams) image.getLayoutParams();
//                float prop= bitmap.getWidth()/bitmap.getHeight();
//                lp.width=bitmap.getWidth();
//                lp.height=bitmap.getHeight();
//                image.setLayoutParams(lp);
            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });
    }

    String getYouTubeID (String url) {

        String pattern = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|watch\\?v%3D|%2Fvideos%2F|embed%‌​2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\n]*";

        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(url);

        if( matcher.find() ){

            String id = matcher.group();

            if ( id.length() == 11 ) {

                return id;

            } else {

                return null;

            }

        } else {

            return null;
        }
    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @Bind(R.id.img_categorylist)
        ImageView imageView;
        @Bind(R.id.text_catelist_imgtitle)
        TextView text;
        @Bind(R.id.ratingBar1)
        RatingBar rating;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
            view.setOnClickListener(this);
        }

        void playVideoYoutube ( Context c, String urlPlayable ) {

            try {

                Intent playReportIntent = new Intent(c, ReproductorYoutube.class);
                playReportIntent.setType(MimeTypeMap.getFileExtensionFromUrl(urlPlayable));
                playReportIntent.setData(Uri.parse( urlPlayable ));
                c.startActivity(playReportIntent);

            } catch (Exception e) {

                Toast.makeText(c, "Error cargando video", Toast.LENGTH_LONG).show();

            }
        }

        @Override
        public void onClick(View view) {
            playVideoYoutube(context,(((LinkedTreeMap<String, String>)item.get(getAdapterPosition())).get("video")));
            AnimeAplication.showInterstitial();
        }
    }
}
