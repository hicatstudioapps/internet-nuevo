package com.hicatstudio.internetfree.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.hicatstudio.internetfree.AnimeAplication;
import com.hicatstudio.internetfree.ProjectDetails_Activity;
import com.hicatstudio.internetfree.R;
import com.hicatstudio.internetfree.model.ProjectsApp;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MyProjectsAppRecyclerViewAdapter extends RecyclerView.Adapter<MyProjectsAppRecyclerViewAdapter.ViewHolder> {

    Context context;
    ArrayList<ProjectsApp> items;

    public MyProjectsAppRecyclerViewAdapter(ArrayList<ProjectsApp> items, Context context) {
        this.items = items;
        this.context=context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_projectsapp, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        ImageLoader.getInstance().displayImage("http://wildercs.net/wilder/app1/images/"+items.get(position).getProjectImage(),holder.imageView);
        holder.text.setText(items.get(position).getProjectName());
        holder.rating.setRating(Float.parseFloat(items.get(position).getRatingPromedio()));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
       @Bind(R.id.img_categorylist)
       ImageView imageView;
        @Bind(R.id.text_catelist_imgtitle)
        TextView text;
        @Bind(R.id.ratingBar1)
        RatingBar rating;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            Intent intent= new Intent(context, ProjectDetails_Activity.class);
            Bundle bundle= new Bundle();
            bundle.putSerializable("o",items.get(getAdapterPosition()));
            intent.putExtras(  bundle);
            context.startActivity(intent);
            AnimeAplication.showInterstitial();
        }
    }
}
