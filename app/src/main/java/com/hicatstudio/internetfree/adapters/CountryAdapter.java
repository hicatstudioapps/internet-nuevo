package com.hicatstudio.internetfree.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hicatstudio.internetfree.AnimeAplication;
import com.hicatstudio.internetfree.ManualActivity;
import com.hicatstudio.internetfree.R;
import com.hicatstudio.internetfree.model.JsonCountryItem;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by CQ on 01/08/2016.
 */
public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.ViewHolder> {

    ArrayList<JsonCountryItem> items;
    Context context;

    public CountryAdapter(ArrayList<JsonCountryItem> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.categoymain_item,null));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        JsonCountryItem data= items.get(position);
        if(data.getCategory_name().equals("Argentina")){
            holder.img_flag.setImageResource(R.drawable.argentina);
            holder.country.setText(data.getCategory_name());
        }
        if(data.getCategory_name().equals("Chile")){
            holder.img_flag.setImageResource(R.drawable.chile);

        }if(data.getCategory_name().equals("Colombia")){
            holder.img_flag.setImageResource(R.drawable.colombia);

        }if(data.getCategory_name().equals("Ecuador")){
            holder.img_flag.setImageResource(R.drawable.ecuador);

        }if(data.getCategory_name().equals("Guatemala")){
            holder.img_flag.setImageResource(R.drawable.guatemala);

        }if(data.getCategory_name().equals("México")){
            holder.img_flag.setImageResource(R.drawable.mexico);

        }if(data.getCategory_name().equals("Nicaragua")){
            holder.img_flag.setImageResource(R.drawable.nicaragua);

        }if(data.getCategory_name().equals("Perú")){
            holder.img_flag.setImageResource(R.drawable.peru);

        }if(data.getCategory_name().equals("United States")){
            holder.img_flag.setImageResource(R.drawable.eu);

        }if(data.getCategory_name().equals("Otros")){
            holder.img_flag.setImageResource(R.drawable.otros);

        }
        holder.country.setText(data.getCategory_name());

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @Bind(R.id.img_flag)
        ImageView img_flag;
        @Bind(R.id.country)
        TextView country;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Bundle data= new Bundle();
            data.putString("cid",items.get(getAdapterPosition()).getCid());
            data.putString("c",items.get(getAdapterPosition()).getCategory_name());
            Intent intent= new Intent(context, ManualActivity.class);
            intent.putExtras(data);
            context.startActivity(intent);
            AnimeAplication.showInterstitial();
        }
    }
}
